#!/usr/bin/env node

const path = require('path');
const root_path = path.join(__dirname);
const db_path = path.join(root_path + '/database.db');
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database(db_path);

db.serialize(function() {
  db.run("\
    CREATE TABLE IF NOT EXISTS users (\
      npm TEXT PRIMARY KEY,\
      name TEXT NOT NULL,\
      saldo INTEGER,\
      timestamp TEXT NOT NULL\
    )\
  ");

  db.run("\
    CREATE TABLE IF NOT EXISTS transfers (\
      id INTEGER PRIMARY KEY,\
      time TEXT,\
      npm_from TEXT,\
      npm_to TEXT,\
      amount INTEGER,\
      status TEXT\
    )\
  ");

  db.run("\
    CREATE TABLE IF NOT EXISTS transfer_locks (\
      id INTEGER PRIMARY KEY,\
      npm TEXT,\
      amount INTEGER,\
      timestamp TEXT\
    )\
  ");

  db.run("\
    CREATE TABLE IF NOT EXISTS pings (\
      npm TEXT PRIMARY KEY,\
      timestamp TEXT,\
      localtime TEXT\
    )\
  ");

  db.run("INSERT OR IGNORE INTO users VALUES ('1406569781', 'George', '1000000000', '2017-10-12 06:12:44')");

  console.log("npm\t:name\t:saldo\t:timestamp");
  db.each("SELECT npm, name, saldo, timestamp FROM users", function(err, row) {
    console.log('users -- ' + row.npm + ": " + row.name + ": " + row.saldo + ": " + row.timestamp);
  });

  console.log("npm\t:timestamp");
  db.each("SELECT npm, timestamp FROM transfer_locks", function(err, row) {
    console.log('locks -- ' + row.id + ': ' + row.npm + ': ' + row.amount + ': ' + row.timestamp);
  });

  console.log("npm\t:timestamp");
  db.each("SELECT npm, timestamp, localtime FROM pings ORDER BY localtime", (err, row) => {
    console.log('pings -- ' + row.npm + ': ' + row.timestamp + ': ' + row.localtime);
  });

  db.all("SELECT name FROM sqlite_master WHERE type='table'", function (err, tables) {
    console.log(tables);
  });

});

db.close();

// db.each("SELECT rowid AS id, info FROM user_info", function(err, row) {
//     console.log(row.id + ": " + row.info);
// });
