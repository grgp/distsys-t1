const path = require('path');
const fs = require('fs');
const request = require('request');

module.exports = {
  pt: (data) => {
    return JSON.stringify(data, null, 2);
  },

  getApiversion: () => {
    const yaml = readYaml.sync('ewallet-v2.3.yaml');
    return yaml.info.version;
  },

  getQuorum: (q) => {
    return new Promise((rslv, rjct) =>
      fs.readFile(path.join(__dirname + '/quorum.json'), 'utf8', (err, data) => {  
        if (err) {  };

        const quorum_local = JSON.parse(data);
        var quorum_count = 0;

        var promises = [];

        Object.keys(quorum_local)
        .filter(npm => { return quorum_local[npm] !== ""; } )
        .map(npm => {
          var options = {
            method: "post",
            body: {},
            json: true,
            url: "http://" + quorum_local[npm] + "/ewallet/ping",
            timeout: 1000
          };

          promises.push(
            new Promise(resolve => {
              request(options, (err, response, body) => {
                if (err) {
                  console.log("Quorum check error to   " + npm + " - " + quorum_local[npm] + " || err: " + err);
                } else if (response.body.pong == 1) {
		  console.log("Quorum check success to " + npm + " - " + quorum_local[npm]);
                  quorum_count++;
                }
                resolve();
              });
            })
          );
        });

        return Promise.all(promises)
          .then(() => {
            console.log("Quorum count after promise: " + quorum_count);
            rslv(quorum_count);
          });
        
        return -1;
      })
    );
  },

  callTransfer: (req) => {
    var options = {
      method: "post",
      body: {"user_id": req.body.user_id,
             "nilai": req.body.nilai},
      json: true,
      url: "http://" + req.body.node_ip + "/ewallet/transfer",
      timeout: 3000
    };

    return new Promise(resolve => {
      request(options, (err, response, body) => {
        if (err) {
          console.log('i got err here');
          resolve(-99);
        } else {
          console.log('i got in here');
          resolve(response.body.status_transfer);
        }
      });
    });
  },

  quorumMap: (json) => {
    var quorumMap = {};
    for (let user of json.arr) {
      console.log(user.npm + ": " + user.ip + "\n");
      quorumMap[user.npm] = user.ip;
    }
    return quorumMap;
  }
}
