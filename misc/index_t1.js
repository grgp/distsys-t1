const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');
const request = require('request');
const bodyParser = require('body-parser');
const readYaml = require('read-yaml');
const sqlite3 = require('sqlite3').verbose();
const hs = require(path.join(__dirname + '/helpers.js'));
const db = new sqlite3.Database(path.join(__dirname + '/database.db'));
const quorum_vals = {
  "getSaldo": 3, "register": 3, "transfer": 3,
  "getTotalSaldo": 6, "total": 6, "debug": 0
}

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.all('*', (req, res, next) => {
  if (['GET', 'POST'].includes(req.method)) {
    next();
  } else {
    res.status(501).send(hs.pt({
      'detail': 'Service will only receive GET and POST',
      'status': 501,
      'title': '501 Not Implemented'
    }));
  }
});

app.all('/ewallet/ping', (req, res, next) => {
//  console.log("/ping is called by: " + req.ip + " - " + req.hostname + " - " + req.ips);
  res.send(hs.pt({ "pong": 1 }));
});

app.post('/ewallet/transfer', (req, res, next) => {
  transfer(req, res, next);
});

app.post('/ewallet_interface/transfer_to', (req, res, next) => {
  hs.getQuorum().then(quorum_count => {
    if (quorum_count < quorum_vals["transfer"]) {
      res.send(hs.pt({ "status_transfer": -2 }));
    } else {
      var request_not_sent = true;
      db.each("SELECT saldo FROM users where npm='" + req.body.user_id + "'",
        (err, row) => {
          request_not_sent = false;
          if (err) {
            res.send(hs.pt({ "status_transfer": -4 }));
          } else if (row.saldo >= req.body.nilai) {
            hs.callTransfer(req).then((status_transfer) => {
              if (status_transfer == 1) {
                req.body.nilai = -1 * req.body.nilai;
                req.body["internal_req"] = true;
                transfer(req, res, next);
              } else {
                res.send(hs.pt({ "status_transfer": status_transfer }));
              }
            });
          } else {
            res.send(hs.pt({ "status_transfer": -5 }));
          }
        },
        (err, num_rows) => {
          if (err) { res.send(hs.pt({ "status_transfer": -4 })); }
          else if (num_rows == 0) { res.send(hs.pt({ "status_transfer": -1 })); }
          else if (request_not_sent) { res.send(hs.pt({ "status_transfer": -99 })); }
        }
      );
    }
  });
});

function transfer(req, res, next) {
  hs.getQuorum().then(quorum_count => {
    if (quorum_count < quorum_vals["transfer"]) {
      res.send(hs.pt({ "status_transfer": -2 }));
    } else {
      const nilai = parseInt(req.body.nilai);
      var request_not_sent = true;

      db.each("SELECT saldo FROM users where npm='" + req.body.user_id + "'",
        (err, row) => { // row callback
          request_not_sent = false;
          if (err) {
            res.send(hs.pt({ "status_transfer": -4 }));
          } else if (!req.body.internal_req && nilai < 0 || nilai > 10e9) { 
            res.send(hs.pt({ "status_transfer": -5 }));
          } else {
            db.run(
              "UPDATE users SET saldo = ? WHERE npm = ?",
              row.saldo + req.body.nilai, req.body.user_id,
              (err) => {
                if (err) {
                  res.send(hs.pt({ "status_transfer": -4 }));
                } else {
                  res.send(hs.pt({ "status_transfer": 1 }));
                  return true;
                }
              }
            );
          }
        },
        (err, num_rows) => { // completion callback
          if (num_rows == 0) {
            res.send(hs.pt({ "status_transfer": -1 }));
          } else if (request_not_sent) {
            res.send(hs.pt({ "status_transfer": -99 }));
          }
        }
      );
    }
  });
  return false;
}

app.post('/ewallet/getSaldo', (req, res, next) => {
  hs.getQuorum().then(quorum_count => {
    if (quorum_count < quorum_vals["getSaldo"]) {
      res.send(hs.pt({ "nilai_saldo": -2 }));
    } else {
      db.each("SELECT saldo FROM users where npm='" + req.body.user_id + "'",
        (err, row) => { // row callback
          res.send(hs.pt({ "nilai_saldo": row.saldo }));
        },
        (err, num_rows) => { // completion callback
          if (err) { res.send(hs.pt({ "nilai_saldo": -4 })); }
          if (num_rows == 0) { res.send(hs.pt({ "nilai_saldo": -1 })); }
        }
      );
    }
  });
});

app.post('/ewallet/getTotalSaldo', (req, res, next) => {
  console.log('Start getTotalSaldo to ' + req.body.user_id);
  hs.getQuorum().then(quorum_count => {
    if (quorum_count < quorum_vals["getTotalSaldo"]) {
      console.log("Quorum count is less than getTotalSaldo, it's " + quorum_count);
      res.send(hs.pt({ "nilai_saldo": -2 }));
    } else {
      console.log("Quorum count is enough for getTotalSaldo");
      db.each("SELECT saldo FROM users where npm='" + req.body.user_id + "'",
        (err, row) => {},
        (err, num_rows) => {
          if (err) { res.send(hs.pt({ "status_transfer": -99 })); }
          else if (num_rows == 0) {
            fs.readFile(path.join(__dirname + '/quorum.json'), 'utf8', (err, data) => {  
              if (err) { }
              else if (quorum_local[req.body.user_id]) {
                const quorum_local = JSON.parse(data);
                var options = {
                  method: "post",
                  body: {"user_id": quorum_local[req.body.user_id]},
                  json: true,
                  url: "http://" + req.body.node_ip + "/ewallet/getTotalSaldo",
                  timeout: 5000
                };

                request(options, (err, response, body) => {
                  if (err) {
                    res.send(hs.pt({ "nilai_saldo": -3 }));
                  } else {
                    res.send(hs.pt( response.body ));
                  }
                });
              } else {
                res.send(hs.pt({ "nilai_saldo": -1 }));
              }
            });
          } else {
            var total_saldo = 0;
            var other_nodes_active = true;
            var all_requests_success = true;
            var user_id_registered = false;
              
            fs.readFile(path.join(__dirname + '/quorum.json'), 'utf8', (err, data) => {  
              if (err) {}
              else {
                const quorum_local = JSON.parse(data);
                var promises = [];

                Object.keys(quorum_local)
                .filter(npm => { return quorum_local[npm] !== ""; } )
                .map(npm => {
                  var options = {
                    method: "POST",
                    body: {"user_id": req.body.user_id},
                    json: true,
                    url: "http://" + quorum_local[npm] + "/ewallet/getSaldo",
                    timeout: 50000
                  };

                  // console.log("Preparing to getSaldo at " + quorum_local[npm]);
                  // console.log("Before pushing to promise at getTotalSaldo");
                  promises.push(
                    new Promise(resolve => {
                      request(options, (err, response, body) => {
                        if (err) {
                          console.log("Request error to " + npm + " || err: " + err);
                          other_nodes_active = false;
                        } else {
                          var ret_saldo = response.body.nilai_saldo;  
                          if (ret_saldo >= 0) {
                            console.log("getSaldo success to " + npm + ": " + response.body.nilai_saldo);
                            total_saldo += ret_saldo;
                          } else {
                              console.log("getSaldo received nilai_saldo code: " + ret_saldo);
                              if (ret_saldo == -99) {
                                all_requests_success = false;
                              }
                          }
                        }
                        resolve();
                      });
                    })
                  );
                  // console.log("After pushing to promise at getTotalSaldo");
                });

                return Promise.all(promises)
                  .then(() => {
                    console.log("All requests success: " + all_requests_success + " || Other nodes active: " + other_nodes_active);
                    if (!all_requests_success) { res.send(hs.pt({ "nilai_saldo": -3 })); }
                    else if (!other_nodes_active) { res.send(hs.pt({ "nilai_saldo": -3 })); }
                    else { res.send(hs.pt({ "nilai_saldo": total_saldo })); }
                  });
              }
            });  
          }
        }
      );
    }
  });
});

function getSaldo(req, res, next) {
  db.each("SELECT saldo FROM users where npm='" + req.body.user_id + "'",
    (err, row) => { // row callback
      res.send(hs.pt({ "nilai_saldo": row.saldo }));
    },
    (err, num_rows) => { // completion callback
      if (err) { res.send(hs.pt({ "nilai_saldo": -4 })); }
      if (num_rows == 0) { res.send(hs.pt({ "nilai_saldo": -1 })); }
    }
  );
}

app.post('/ewallet/register', (req, res, next) => {
  const d = new Date();
  hs.getQuorum().then(quorum_count => {
    if (quorum_count < quorum_vals["register"]) {
      res.send(hs.pt({ "status_register": -2 }));
    } else {
      db.run(
        "INSERT INTO users VALUES ($user_id, $user_name, $saldo, $time)", {
          $user_id: req.body.user_id,
          $user_name: req.body.nama,
          $saldo: 0,
          $time: d.toISOString()
        },
        (err) => {
          var status_register = 1;
          if (err) { status_register = -4; }
          res.send(hs.pt({ "status_register": status_register }));
        }
      );
    }
  });
});

app.get('/ewallet/updateQuorum', (req, res) => {
  fs.readFile(path.join(__dirname + '/quorum.json'), 'utf8', (err, data) => {  
    if (err) { res.send(hs.pt({"message": -1})) };

    var quorum_local = JSON.parse(data);
    var quorum_repo;
    request('http://172.17.0.59:8000/quorum.json', (err, response, body) => {
      if (err) {} else {
        quorum_repo = hs.quorumMap(JSON.parse('{"arr":'+body+'}'));

        Object.keys(quorum_local).map(npm => {
          quorum_local[npm] = quorum_repo[npm];
        });

        const quorum_pretty = hs.pt(quorum_local);
        fs.writeFile('quorum.json', quorum_pretty, "utf-8");
        res.send(quorum_pretty);
      }
    });
  });
});

app.get('/', (req, res) => {
  res.redirect('/home');
});

app.get('/home', (req, res) => {
  fs.readFile(path.join(__dirname + '/public/home.html'), 'utf8', (err, data) => {  
    if (err) throw err;
    res.send(data);
  });
});

app.listen(8080, () => {
  console.log('Server listening on port 8080 through nginx on port 80!');
});
