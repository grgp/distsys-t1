app.get('/style', function (req, res) {
  res.sendFile(path.join(__dirname + '/public/style.css'));
});

app.get('/info', function (req, res) {
  const query = req.query.type;
  if (query == 'time') {
    res.send(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''));
  }
});

app.post('/home', function (req, res) {
  fs.readFile(path.join(__dirname + '/public/home.html'), 'utf8', function(err, data) {  
    if (err) throw err;
    data = data.replace('__HELLO__', req.body.name);
    res.send(data);
  });
});

//##########################
// Homework 3
//##########################

app.all('/api/hello', function (req, res, next) {
  if (req.method == 'POST') {
    next();
  } else {
    res.status(405).send({
      'detail': '/api/hello can only receive certain POST requests',
      'status': 405,
      'title': 'Method not allowed'
    });
  }
});

app.post('/api/hello', function (req, res) {
  if (!req.body.request) {
    res.status(400).send({
      'detail': "'request' is a required property",
      'status': 400,
      'title': 'Bad Request'
    });
  } else {
    fs.readFile(path.join(__dirname + '/data.json'), 'utf8', function (err, data) {
      if (err) throw err;
      json = JSON.parse(data);
      json.count += 1;

      fs.writeFile(path.join(__dirname + '/data.json'), JSON.stringify(json), function (err) {
        if (err) throw err;
      });

      request('http://172.17.0.70:17088', function (error, response, body) {
        body = JSON.parse(body);
        if (!error) {
          res.send({
            'apiversion': getApiversion(),
            'count': json.count,
            'currentvisit': body.datetime,
            'response': 'Good ' + body.state + ', ' + req.body.request
          });
        }
      });

      // res.send({ // test on local
      //   'apiversion': getApiversion(),
      //   'count': json.count,
      //   'currentvisit': new Date(),
      //   'response': 'Good ' + 'day'
      // });
    }); 
  }
});

app.get('/api/plus_one/:number', function (req, res) {
  if (req.method == "GET" && req.params.number.match(/^\d+$/)) {
    res.send({
      'apiversion': getApiversion(),
      'plusoneret': parseInt(req.params.number) + 1
    });
  } else {
    res.status(404).send({
      "detail": "The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.",
      "status": 404,
      "title": "Not Found"
    });
  }
});
