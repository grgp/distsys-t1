const config = require('../config');
const helpers = require('../helpers');
const api = require('../api');
const transferUtils = require('../api/transferUtils');
const pb = require('./publisher');
const amqp = require('amqplib/callback_api');
const cred = require('amqplib/lib/credentials');
const options = {
  credentials: cred.plain(config.amqp_user, config.amqp_pass),
};

module.exports = {
  consume: (ex) => {
    amqp.connect(config.amqp_address, options, (err, conn) => {
      conn.createChannel((err, ch) => {
        ch.assertQueue('', {exclusive: true}, function(err, q) {
          console.log(' [*] Waiting for logs on exchange ' + ex + '. CTRL+C to exit.');
    
          ch.bindQueue(q.queue, ex, config.request_key);
          ch.bindQueue(q.queue, ex, config.response_key);
    
          ch.consume(q.queue, (msg) => {
            const obj = JSON.parse(msg.content.toString());
            console.log(' [>] request: ' + JSON.stringify(obj));
            // console.log(" [x] %s: '%s'", msg.fields.routingKey, msg.content.toString()) 
            if (obj.type === 'request') {
              api[obj.action](obj).then(response => {
                response.action = obj.action;
                response.type = 'response';
                response.ts = helpers.getTimestamp();

                pb.publish(response, obj.sender_id);
              });
            } else if (obj.type === 'response') {
              if (obj.action === 'transfer') {
                transferUtils.applyTransfer(obj).then(status => {
                  console.log(' [L] local: ' + JSON.stringify(status));
                });
              } else {
                console.log(JSON.stringify(obj));
              }
            }
          }, {noAck: true});
        });
      });
    });
  },
};