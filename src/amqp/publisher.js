const config = require('../config');
const amqp = require('amqplib/callback_api');
const cred = require('amqplib/lib/credentials');
const options = {
  credentials: cred.plain(config.amqp_user, config.amqp_pass),
};

module.exports = {
  publish: (data, send_to) => {
    amqp.connect(config.amqp_address, options, (err, conn) => {
      conn.createChannel((err, ch) => {
        const ex = config.ex_map[data.action];
        const dataString = JSON.stringify(data);
        const routing_key = (data.type === 'request' ? 'REQ_':'RESP_') + send_to;
    
        ch.assertExchange(ex, 'direct', {durable: true});
        ch.publish(ex, routing_key, new Buffer(dataString));
        console.log(" [x] Sent %s: '%s'", routing_key, dataString);
      });
    
      // setTimeout(() => { conn.close(); process.exit(0) }, 500);
    });    
  },
};
