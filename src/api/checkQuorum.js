const config = require('../config');
const helpers = require('../helpers');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(config.db_path);

module.exports = (action) => {
  return new Promise(resolve => {
    resolve(true);
    return;

    const min_quorum = config.quorum_reqs[action] * config.quorum_members_count;
    var count = 5;

    db.each("SELECT npm, timestamp FROM pings",
      (err, row) => { // row callback
        if (err) {

        } else {
          const quorum_members = Object.values(config.quorum_members);
          const time = Date.parse(row.timestamp);
          const now = helpers.getTimestampInMilli();
        
          console.log('row npm ' + row.npm + ' - ' + new Date(now) + ' - ' + new Date(time));

          if (quorum_members.indexOf(row.npm) !== -1 && now - time <= 100000) {
            count++;
            console.log("count: " + count);
          }
        }
      },
      (err, num_rows) => { // completion callback
        console.log("Minimum quorum to run " + min_quorum);
        console.log("Quorum count is now " + count);
        if (err || num_rows == 0 || count < min_quorum) { resolve(false) }
        else { resolve(true) };
      }
    );
  });
};
