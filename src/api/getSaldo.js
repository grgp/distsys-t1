const config = require('../config');
const checkQuorum = require('./checkQuorum');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(config.db_path);

function getSaldo(obj) {
  return new Promise(resolve => {
    db.each("SELECT saldo FROM users where npm='" + obj.user_id + "'",
      (err, row) => { // row callback
        resolve({ "nilai_saldo": row.saldo });
      },
      (err, num_rows) => { // completion callback
        if (err) { resolve({ "nilai_saldo": -4 }) };
        if (num_rows == 0) { resolve({ "nilai_saldo": -1 }) };
      }
    );
  });
}

module.exports = (obj) => {
  return checkQuorum(obj.action).then(rslv => {
    if (!rslv) {
      return new Promise(resolve => { resolve({ "nilai_saldo": -2 }) } );
    } else {
      return getSaldo(obj);
    }
  });
};