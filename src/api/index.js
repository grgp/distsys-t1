const config = require('../config');
const register = require('./register');
const transfer = require('./transfer');
const getSaldo = require('./getSaldo');
const getTotalSaldo = require('./getTotalSaldo');

module.exports = {
  register: register,
  transfer: transfer,
  get_saldo: getSaldo,
  get_total_saldo: getTotalSaldo,
};