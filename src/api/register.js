const config = require('../config');
const helpers = require('../helpers');
const checkQuorum = require('./checkQuorum');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(config.db_path);

function register(obj) {
  return new Promise(resolve => {
    const d = new Date();
    db.run(
      "INSERT INTO users VALUES ($user_id, $user_name, $saldo, $timestamp)", {
        $user_id: obj.user_id,
        $user_name: obj.nama,
        $saldo: 0,
        $timestamp: helpers.getTimestamp()
      },
      (err) => {
        var status_register = 1;
        if (err) { status_register = -4; }
        resolve({ "status_register": status_register });
      }
    );
  });
}

module.exports = (obj) => {
  return checkQuorum(obj.action).then(rslv => {
    if (!rslv) {
      return new Promise(resolve => { resolve({ "status_register": -2 }) } );
    } else {
      return register(obj);
    }
  });
};