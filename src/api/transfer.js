const config = require('../config');
const helpers = require('../helpers');
const checkQuorum = require('./checkQuorum');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(config.db_path);

function transfer(obj) {
  return new Promise(resolve => {
    if (!helpers.isNumeric(obj.nilai)) {
      resolve({ "status_transfer": -5 });
      return;
    }

    const nilai = parseInt(obj.nilai);
    var request_not_sent = true;

    db.each("SELECT saldo FROM users where npm='" + obj.user_id + "'",
      (err, row) => { // row callback
        request_not_sent = false;
        if (err) {
          resolve({ "status_transfer": -4 });
        } else if (!obj.internal_req && nilai < 0 || nilai > 10e9) { 
          resolve({ "status_transfer": -5 });
        } else {
          db.run(
            "UPDATE users SET saldo = ? WHERE npm = ?",
            row.saldo + nilai, obj.user_id,
            (err) => {
              if (err) {
                resolve({ "status_transfer": -4 });
              } else {
                resolve({ "status_transfer": 1 });
              }
            }
          );
        }
      },
      (err, num_rows) => { // completion callback
        if (num_rows == 0) {
          resolve({ "status_transfer": -1 });
        } else if (request_not_sent) {
          resolve({ "status_transfer": -99 });
        }
      }
    );
  });
}

module.exports = (obj) => {
  return checkQuorum(obj.action).then(rslv => {
    if (!rslv) {
      return new Promise(resolve => { resolve({ "status_transfer": -2 }) } );
    } else {
      return transfer(obj);
    }
  });
};