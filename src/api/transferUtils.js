const config = require('../config');
const helpers = require('../helpers');
const transfer = require('./transfer');
const checkQuorum = require('./checkQuorum');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(config.db_path);

module.exports = {
  lockFromTransfers: (obj) => {
    return new Promise(resolve => {
      db.run(
        "INSERT OR REPLACE INTO transfer_locks VALUES ($id, $npm, $amount, $timestamp)", {
          $id: 1,
          $npm: config.sender_id,
          $amount: obj.nilai,
          $timestamp: helpers.getTimestamp()
        },
        (err) => {
          var transfer_locked = 1;
          if (err) { transfer_locked = -4; }
          resolve({ "transfer_locked": transfer_locked });
        }
      );
    });
  },

  releaseTransfersLock: (obj) => {
    return new Promise(resolve =>  {
      db.run("DELETE FROM transfer_locks", (err) => {
        if (err) {
          resolve({ "saldo_updated": -99 });
        } else {
          resolve({ "saldo_updated": -5 });
        }
      });
    })
  },

  applyTransfer: (obj) => {
    return new Promise(resolve => {
      db.each("SELECT npm, amount FROM transfer_locks",
        (err, row) => { // row callback
          obj.sender_id = config.sender_id;
          obj.user_id = row.npm;
          obj.nilai = -parseInt(row.amount);
          obj.internal_req = true;
          transfer(obj).then(status => {
            db.run("DELETE FROM transfer_locks", (err) => {
              if (err) {
                resolve({ "saldo_updated": -99 });
              } else {
                if (status['status_transfer'] == 1) {
                  resolve({ "saldo_updated": 1 });
                } else {
                  resolve({ "saldo_updated": -5 });
                }
              }
            });
          });
        },
        (err, num_rows) => { // completion callback
          db.run("DELETE FROM transfer_locks", (err_db) => {
            if (err || err_db) { resolve({ "saldo_updated": -99 }) };
            if (num_rows == 0) { resolve({ "saldo_updated": -1 }) };
          });
        }
      );
    });
  },
}