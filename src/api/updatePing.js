const config = require('../config');
const helpers = require('../helpers');
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database(config.db_path);

module.exports = (obj) => {
  return new Promise(resolve => {
    db.run(
      "INSERT OR REPLACE INTO pings VALUES ($npm, $timestamp, $localtime)", {
        $npm: obj.npm,
        $timestamp: obj.ts,
        $localtime: helpers.getTimestamp()
      },
      (err) => {
        var ping_update = 1;
        if (err) { ping_update = -99; }
        resolve(ping_update);
      }
    );
  });
};