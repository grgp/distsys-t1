const path = require('path');
const root_path = path.join(__dirname + '/..');
const db_path = path.join(root_path + '/database.db');

module.exports = {
  // amqp_address: 'amqp://localhost',
  // amqp_user: 'guest',
  // amqp_pass: 'guest',
  amqp_address: 'amqp://172.17.0.3',
  amqp_user: 'sisdis',
  amqp_pass: 'sisdis',
  db_path: db_path,
  ex_map: {
    'register': 'EX_REGISTER',
    'transfer': 'EX_TRANSFER',
    'get_saldo': 'EX_GET_SALDO',
    'get_total_saldo': 'EX_GET_TOTAL_SALDO',
  },
  sender_id: '1406569781',
  request_key: 'REQ_1406569781',
  response_key: 'RESP_1406569781',
  root_path: root_path,
  quorum_members_count: 5,
  quorum_reqs: {
    'register': 0.5,
    'transfer': 0.5,
    'get_saldo': 0.5,
    'get_total_saldo': 0.99
  },
  quorum_members: {
    'me': '1406569781',
    'faj': '1406543605',
    'ag': '1406559042',
    'azm': '1406559061',
    'wis': '1406573356',
  },
};
