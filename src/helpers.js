module.exports = {
  getTimestamp: () => {
    const d = new Date();
    return d.toISOString().replace(/\..*/, '').replace(/T/, ' ');
  },

  getTimestampInMilli: () => {
    const d = new Date();
    return d.getTime();
  },

  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
}