const config = require('./config');
const cn = require('./amqp/consumer');

for (exchange of Object.values(config.ex_map)) {
  cn.consume(exchange);
};