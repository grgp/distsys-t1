#!/usr/bin/env node

const config = require('./config');
const d = new Date();
const pb = require('./amqp/publisher');
const transferUtils = require('./api/transferUtils');
const args = process.argv.slice(2);
const send_to = args[0];

var data = {
  action: args[1].toLowerCase(),
  user_id: args[2],
  sender_id: config.sender_id,
  type: 'request',
  ts: d.toISOString().replace(/\..*/, '').replace(/T/, ' ')
}

if (data.action == 'register' && args.length >= 4) {
  data.nama = args.slice(3).join(' ');
  pb.publish(data, send_to);
} else if (data.action == 'transfer' && args.length == 4) {
  data.nilai = args[3];
  transferUtils.lockFromTransfers(data);
  pb.publish(data, send_to);
} else if (data.action == 'get_saldo' && args.length == 3) {
  pb.publish(data, send_to);
} else if (data.action == 'get_total_saldo' && args.length == 3) {
  pb.publish(data, send_to);
} else {
  console.log('Call this interface by this format:');
  console.log('node interface.js <routing_key_npm> <action> <user_id> ?optional <transfer> ?or? <nama>');
  console.log('\nExamples:');
  console.log('node interface.js 1406512351 transfer 1406569781 45000');
  console.log('node interface.js 1406512351 register 1406569781 George Albert');
}

setTimeout(() => { process.exit(0) }, 1500);
