const config = require('../config');
const helpers = require('../helpers');
const amqp = require('amqplib/callback_api');
const cred = require('amqplib/lib/credentials');
const options = {
  credentials: cred.plain(config.amqp_user, config.amqp_pass),
};

module.exports = () => 
  amqp.connect(config.amqp_address, options, (err, conn) => {
    if (err) {
      console.log("Error in connecting to 172.17.0.3:5672 " + err);
    }
    
    conn.createChannel((err, ch) => {
      const d = new Date();
      const ex = 'EX_PING';
      const q = '';
      const msg_obj = {
        action: 'ping',
        npm: '1406569781',
        ts: helpers.getTimestamp()
      };

      const msg_json = JSON.stringify(msg_obj);

      ch.assertExchange(ex, 'fanout', {durable: false});

      setInterval(() => {
        ch.publish(ex, '', new Buffer(msg_json));
        // console.log(" [x] Sent %s", msg_json);
      }, 5000);        

      // ch.assertQueue(q, {durable: false});
      // Note: on Node 6 Buffer.from(msg) should be used
      // ch.sendToQueue(q, new Buffer(msg_json));
    });
    // setTimeout(() => { conn.close(); process.exit(0) }, 500);
  });
